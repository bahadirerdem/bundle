package com.example.bahadirerdem.bundle.Control

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_tabbed.*
import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import android.view.ViewGroup
import com.example.bahadirerdem.bundle.API.Config
import com.example.bahadirerdem.bundle.CheckInternet.ConnectivityReceiver
import com.example.bahadirerdem.bundle.Fragments.NotificationFragment
import com.example.bahadirerdem.bundle.Fragments.SavedArticlesFragment
import com.example.bahadirerdem.bundle.R


class TabbedActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabbed)
        registerReceiver(
            ConnectivityReceiver(),
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        textView.setOnClickListener {
            Toast.makeText(this,"Giriş Yap",Toast.LENGTH_SHORT).show()
        }
        imageView.setOnClickListener {
            Toast.makeText(this,"Giriş Yap",Toast.LENGTH_SHORT).show()
        }
        imageSettings.setOnClickListener {
            Toast.makeText(this,"Ayarlar",Toast.LENGTH_SHORT).show()
        }
    }

    fun fragmentSet(){
        val adapter:ViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        adapter.AddFragment(NotificationFragment(),"Bildirimler")
        adapter.AddFragment(SavedArticlesFragment(),"Kayıtlı Haberler")
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
        setCustomFont()
    }
    fun setCustomFont() {
        val vg = tabLayout.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup
            val tabChildsCount = vgTab.childCount
            for (i in 0 until tabChildsCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView) {
                    tabViewChild.typeface = Typeface.createFromAsset(assets, "RobotoCondensed-Bold.ttf")
                }
            }
        }
    }
    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {
            Config.internetStatus = false
            offlineConstraint.visibility = View.VISIBLE
            fragmentSet()

        } else {
            Config.internetStatus = true
            offlineConstraint.visibility = View.GONE
            fragmentSet()
        }
    }
    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }
}
