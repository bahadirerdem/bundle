package com.example.bahadirerdem.bundle.RealmDB;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class DatasModel extends RealmObject {

    // kotlin ile yazdığımda Cause: io.realm.com_bahadirerdem_bundleassignment_RealmDB_DatasModelRealmProxylongerface hatasını alıyordum
    // ve java ile yazdığımda herhangi bir hata durumu olmadığı için burada zaman kaybı olmaması için java kullanmayı tercih ettim
    // en altta kotlin kodları mevcuttur.
    @PrimaryKey
    private long id;
    private String title;
    private String time;
    private String subject;
    private String content;


    public DatasModel() {
    }

    public DatasModel(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

/*

@RealmClass
open class DatasModel : RealmObject {
 @PrimaryKey
    var id: long = 0
    var title: String? = null
    var time: String? = null
    var subject: String? = null
    var content: String? = null

    constructor() {}
    constructor(id: long) {
        this.id = id

    }
 */


