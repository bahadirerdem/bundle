package com.example.bahadirerdem.bundle.API


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface{
    @GET("notification")
    fun firstRequest(@Query("countryId") countryId: Int,@Query("langCode") langCode: String,@Query("platform") platform: Int,@Query("includePictures") includePictures: Int): Call<Model>

    @GET("notification")
    fun paginationRequest(@Query("countryId") countryId: Int,@Query("langCode") langCode: String,@Query("platform") platform: Int,@Query("includePictures") includePictures: Int,@Query("lastId") lastId: String): Call<Model>

}