package com.example.bahadirerdem.bundle.Control

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.bahadirerdem.bundle.API.Config
import com.example.bahadirerdem.bundle.R
import kotlinx.android.synthetic.main.activity_news_webview.*

class NewsWebview : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_webview)

        if (!Config.webviewUrl.isEmpty()) {
            webView.settings.javaScriptEnabled = true
            webView.settings.setSupportZoom(true)
            webView.settings.builtInZoomControls = true
            webView.settings.displayZoomControls = false
            webView.loadDataWithBaseURL(null, Config.webviewUrl, "text/html", "UTF-8", null)
        }
    }
}
