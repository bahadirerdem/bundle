package com.example.bahadirerdem.bundle.Adapter


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.example.bahadirerdem.bundle.API.Config
import com.example.bahadirerdem.bundle.Control.NewsWebview
import com.example.bahadirerdem.bundle.R


class RealmAdapter(private val context: Context) : RecyclerView.Adapter<RealmAdapter.RealmAdapterViewHolder>() {


    inner class RealmAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var titleText: TextView
        internal var subjectText: TextView
        internal var timeText: TextView


        init {
            titleText = itemView.findViewById(R.id.titleText)
            subjectText = itemView.findViewById(R.id.subjectText)
            timeText = itemView.findViewById(R.id.timeText)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RealmAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_list_notification, parent, false);
        return RealmAdapterViewHolder(view)
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RealmAdapterViewHolder, position: Int) {

        Config.titleSetText(Config.realmDbModel[position].title, holder.titleText)
        holder.subjectText.text = Config.realmDbModel[position].subject
        Config.dateCalc(Config.realmDbModel[position].time, holder.timeText)

        val animation: Animation = AnimationUtils.loadAnimation(context, R.anim.item_list_animation)
        holder.itemView.startAnimation(animation)

        holder.itemView.setOnClickListener {
            Config.webviewUrl = Config.realmDbModel[position].content
            val intent = Intent(context, NewsWebview::class.java)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return Config.realmDbModel.size
    }
}
