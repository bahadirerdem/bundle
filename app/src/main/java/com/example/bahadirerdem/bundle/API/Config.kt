package com.example.bahadirerdem.bundle.API

import android.annotation.SuppressLint
import android.widget.TextView
import com.example.bahadirerdem.bundle.RealmDB.DatasModel
import java.text.SimpleDateFormat
import java.util.*


object Config {
    val countryId = 228
    val langCode = "TR"
    val platform = 2
    val includePictures = 1
    var webviewUrl = ""
    var model = mutableListOf<Item>()
    var realmDbModel = mutableListOf<DatasModel>()
    var internetStatus = false





    @SuppressLint("SetTextI18n")
    fun titleSetText(channelName: String, titleText: TextView) {
        when {
            channelName == "breaking_news" -> titleText.text = "Son Dakika"
            channelName == "lifestyle" -> titleText.text = "Yaşamın İçinden"
            channelName == "hot_bundle" -> titleText.text = "Hot Bundle"
            channelName == "sports" -> titleText.text = "Spor"
            channelName == "technology" -> titleText.text = "Bilim & Teknoloji"
            else -> titleText.text = "Diğer"
        }

    }
    @SuppressLint("SetTextI18n")
    fun dateCalc(time: String, timeText: TextView) {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
        val date1 = format.parse(time).time
        val date = Calendar.getInstance()
        val t = date.timeInMillis - 10800000
        val milliSecond = t - date1
        val second = (milliSecond / 1000) % 60
        val minute = (milliSecond / (1000 * 60) % 60)
        val hour = (milliSecond / (1000 * 60 * 60) % 24)
        val day = (milliSecond / (60 * 60 * 24 * 1000))
        val week = day / 7
        val month = day / 30
        val year = month / 12
        when {
            year.toInt() != 0 -> timeText.text = "$year Yıl"
            month.toInt() != 0 -> timeText.text = "$month Ay"
            week.toInt() != 0 -> timeText.text = "$week Hafta"
            day.toInt() != 0 -> timeText.text = "$day Gün"
            hour.toInt() != 0 -> timeText.text = "$hour Saat"
            minute.toInt() != 0 -> timeText.text = "$minute Dakika"
            second.toInt() != 0 -> timeText.text = "$second Saniye"
            else -> timeText.text = ""
        }
    }
}