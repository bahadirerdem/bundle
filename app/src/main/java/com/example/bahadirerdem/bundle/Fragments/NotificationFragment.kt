package com.example.bahadirerdem.bundle.Fragments


import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bahadirerdem.bundle.API.*
import com.example.bahadirerdem.bundle.Adapter.CustomAdapter
import com.example.bahadirerdem.bundle.Adapter.RealmAdapter
import com.example.bahadirerdem.bundle.R
import com.example.bahadirerdem.bundle.RealmDB.DatasModel
import io.realm.Realm
import io.realm.RealmConfiguration
import kotlinx.android.synthetic.main.fragment_notification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationFragment : Fragment() {

    private var previousTotal = 0
    private var loading = true
    private var visibleThreshold = 2
    private var firstVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_notification, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = CustomAdapter(activity!!.applicationContext)
        val realmAdapter = RealmAdapter(activity!!.applicationContext)

        Realm.init(activity!!)
        val configuration = RealmConfiguration.Builder().name("bundle.realm").build()
        Realm.getInstance(configuration)
        val realm = Realm.getDefaultInstance()

        recyclerview.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerview.layoutManager = layoutManager
        recyclerview.addItemDecoration(DividerItemDecoration(recyclerview.context, DividerItemDecoration.VERTICAL))
        progressBar.visibility = View.VISIBLE

        if (Config.internetStatus) {
            internetSuccessCall(adapter,realm,realmAdapter)
        } else {
           internetFailureCall(realmAdapter, realm)
        }

    }

    private fun internetSuccessCall (adapter : CustomAdapter, realm: Realm, realmAdapter: RealmAdapter){
        recyclerview.adapter = null
        val apiService = ApiClient.client!!.create(ApiInterface::class.java)
        val call =
            apiService.firstRequest(Config.countryId, Config.langCode, Config.platform, Config.includePictures)
        call.enqueue(object : Callback<Model> {
            override fun onResponse(call: Call<Model>?, response: Response<Model>?) {

                if (response?.body()?.IsSuccess == true) {
                    if (response.body()?.Data?.Items?.size ?: 0 != 0) {
                        val model = response.body()!!.Data.Items
                        Config.model = model as MutableList<Item>
                        recyclerview.adapter = adapter
                        progressBar.visibility = View.GONE
                        addOrUpdate(realm)
                    }
                } else {
                    Log.e("HATA KODU", " =".plus(response?.code()))
                }
            }

            override fun onFailure(call: Call<Model>?, t: Throwable?) {
                Log.e("Error", "Failure=".plus(t?.message.toString()))
                internetFailureCall(realmAdapter, realm) // herhangi bir hatada veritabanındaki verileri parse eder
            }

        })
        loading = true

        recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    visibleItemCount = recyclerview.childCount
                    totalItemCount = layoutManager.itemCount
                    firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false
                            previousTotal = totalItemCount
                        }
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)
                ) {
                    progressBar.visibility = View.VISIBLE

                    val apiServicePagination = ApiClient.client!!.create(ApiInterface::class.java)
                    val lastId = Config.model[Config.model.size - 1].NewsDetail.RssDataID
                    val callPagination = apiServicePagination.paginationRequest(
                        Config.countryId,
                        Config.langCode,
                        Config.platform,
                        Config.includePictures,
                        lastId
                    )
                    callPagination.enqueue(object : Callback<Model> {
                        override fun onResponse(call: Call<Model>?, response: Response<Model>?) {

                            if (response?.body()?.IsSuccess == true) {
                                if (response.body()?.Data?.Items?.size ?: 0 != 0) {
                                    val model = response.body()!!.Data.Items
                                    for (item: Item in model) {
                                        Config.model.add(item)
                                    }
                                    addOrUpdate(realm)
                                    adapter.notifyDataSetChanged()
                                    progressBar.visibility = View.GONE
                                }
                            } else {
                                Log.e("HATA KODU", " =".plus(response?.code()))
                            }
                        }

                        override fun onFailure(call: Call<Model>?, t: Throwable?) {
                            Log.e("Error", "Failure=".plus(t?.message.toString()))
                        }
                    })
                    loading = true
                }
            }
        })
    }
    private fun internetFailureCall(realmAdapter: RealmAdapter, realm: Realm){
        recyclerview.adapter = null
        dataRead(realm)
        recyclerview.adapter = realmAdapter
        progressBar.visibility = View.GONE
        previousTotal = 0
        loading = true
        visibleThreshold = 2
        firstVisibleItem = 0
        visibleItemCount = 0
        totalItemCount = 0
    }
    private fun addOrUpdate(realm: Realm) {
        for (i in 0 until Config.model.size) {
            realm.beginTransaction()
            val data = DatasModel(i.toLong())
            data.content = Config.model[i].NewsDetail.Content
            data.title = Config.model[i].NewsDetail.NotificationChannelCategoryLocalizationKey
            data.subject = Config.model[i].NewsDetail.Title
            data.time = Config.model[i].NewsDetail.PubDate
            realm.insertOrUpdate(data)
            realm.commitTransaction()
        }


    }
    private fun dataRead(realm: Realm) {
        Config.realmDbModel.clear()
        val allData = realm.where(DatasModel::class.java).findAll()
        if (allData.size == 0) {
            alertDatabase()
        } else {
            allData.forEach { data ->
                Config.realmDbModel.add(data)
            }
        }
    }
    private fun alertDatabase() {
        val alert: AlertDialog.Builder = AlertDialog.Builder(context)
        alert.setMessage("Uygulamaya en az bir kere internetiniz açık bir şekilde giriş yapmanız gerekmektedir")
            .setCancelable(false)
        alert.setPositiveButton("Tamam") { evetdialog, evetwhich ->
            activity?.finish()
        }
        val alertDialog: AlertDialog = alert.create()
        alertDialog.setTitle("İnternet Bağlantısı")
        alertDialog.show()
    }

}
