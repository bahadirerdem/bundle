package com.example.bahadirerdem.bundle.Adapter


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.bahadirerdem.bundle.API.Config
import com.example.bahadirerdem.bundle.Control.NewsWebview
import com.example.bahadirerdem.bundle.R


class CustomAdapter(private val context: Context) : RecyclerView.Adapter<CustomAdapter.CustomAdapterViewHolder>() {


    inner class CustomAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var titleText: TextView
        internal var subjectText: TextView
        internal var timeText: TextView


        init {
            titleText = itemView.findViewById(R.id.titleText)
            subjectText = itemView.findViewById(R.id.subjectText)
            timeText = itemView.findViewById(R.id.timeText)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_list_notification, parent, false);
        return CustomAdapterViewHolder(view)
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int) {

        Config.titleSetText(
            Config.model[position].NewsDetail.NotificationChannelCategoryLocalizationKey,
            holder.titleText
        )
        holder.subjectText.text = Config.model[position].NewsDetail.Title
        Config.dateCalc(Config.model[position].NewsDetail.PubDate, holder.timeText)

        holder.itemView.setOnClickListener {
            Config.webviewUrl = Config.model[position].NewsDetail.Content
            val intent = Intent(context, NewsWebview::class.java)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return Config.model.size
    }

}
