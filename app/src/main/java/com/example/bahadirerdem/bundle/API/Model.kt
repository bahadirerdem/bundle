package com.example.bahadirerdem.bundle.API


data class Model(
    val Data: Data,
    val ErrorMsg: String,
    val IsSuccess: Boolean
)

data class Data(
    val FollowersCount: Int,
    val Items: List<Item>
)

data class Item(
    val AddButton: Boolean,
    val Announcement: Boolean,
    val AnnouncementColorCode: Any,
    val AnnouncementText: Any,
    val BannerAd: Boolean,
    val BundleTag: Any,
    val FollowerCount: Any,
    val IsDailyDigest: Boolean,
    val LogoVersion: Any,
    val NewsDetail: NewsDetail,
    val PushType: Int,
    val ReadMode: Boolean,
    val SmallBannerAd: Boolean,
    val bundlepartner: Boolean
)

data class NewsDetail(
    val ChannelCategoryID: Int,
    val ChannelCategoryLocalizationKey: String,
    val Content: String,
    val CountryID: Int,
    val ImageDetail: ImageDetail,
    val Link: String,
    val NewsChannelID: Int,
    val NewsChannelName: String,
    val NotificationChannelCategoryLocalizationKey: String,
    val PubDate: String,
    val RssDataID: String,
    val ShareUrl: String,
    val Title: String,
    val WriterChannelCategory: Any
)

data class ImageDetail(
    val iPad: Any,
    val iPhone: Any
)